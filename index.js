console.log("Hello World!");

let trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasuar"];
trainer.friends = {
  kanto: ["Brock", "Misty"],
  hoenn: ["May", "Max"],
};

trainer.talk = function () {
  console.log("Pikachu, I choose you!");
};

// Create a constructor function for creating a pokemon
function pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  // Methods
  // Will accept an object as a target
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name);
    target.health -= this.attack;
    console.log(target.name + "'s health is now reduced to " + target.health);

    if (target.health <= 0) {
      target.faint();
    }
  };

  this.faint = function () {
    console.log(this.name + " fainted.");
  };
}

let pikachu = new pokemon("Pikachu", 12);
console.log(pikachu);

let mewto = new pokemon("Mewto", 100);
console.log(mewto);

mewto.tackle(pikachu);
console.log(pikachu);
